import { Component, Input, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { v4 } from 'uuid';
import { AppService } from '../app.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnChanges {
  @Input() type: string;
  @Input() task: any;
  form: FormGroup;
  initialFormValue: any;
  constructor(
    private fb: FormBuilder,
    private appService: AppService
  ) {
    this.form = this.fb.group({
      id: [v4()],
      title: [undefined, Validators.required],
      description: [],
      dueDate: [new Date(), [Validators.required]],
      piority: ['normal', Validators.required],
    });
    this.initialFormValue = this.form.value;
  }

  myFilter = (d: Date | null): boolean => {
    const date = new Date();
    return new Date(d) >= new Date(date.setDate(date.getDate() - 1));
  }

  ngOnChanges(): void {
    if (this.task) {
      this.form.patchValue(this.task);
    }
  }

  save(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    const tasks = this.appService.getTask();
    if (this.type === 'add') {
      this.appService.taskList$.next([...tasks, this.form.value]);
      this.form.reset({
        ...this.initialFormValue,
        id: v4()
      });
    } else {
      const index = tasks.findIndex(task => task.id === this.task.id);
      tasks[index] = this.form.value;
      this.appService.taskList$.next(tasks);
    }
  }

}
