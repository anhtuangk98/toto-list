import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Task } from './app.model';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  taskList$ = new BehaviorSubject<any[]>(this.getTask());
  constructor() { }

  getTask(): Task[] {
    const task = JSON.parse(localStorage.getItem('tasks'));
    if (Array.isArray(task) && task.length) {
      return task;
    }
    return [];
  }

  setTask(task): void {
    localStorage.setItem('tasks', JSON.stringify(task));
  }
}
