import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AppService } from './app.service';
import { Task } from './app.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  tasks: Task[] = [];
  filteredTask: Task[] = [];
  search: FormControl = new FormControl('');
  selectedTaskIds = [];
  destroy$ = new Subject();
  constructor(private appService: AppService) {
    this.appService.taskList$
      .asObservable()
      .pipe(takeUntil(this.destroy$))
      .subscribe((tasks) => {
        this.sortTask(tasks);
        this.filterTask(this.search.value);
        this.storeTask();
      });
  }

  ngOnInit(): void {
    this.search.valueChanges.subscribe((key) => {
      this.filterTask(key);
    });
  }

  sortTask(tasks: Task[]): void {
    this.tasks = tasks.sort((a: Task, b: Task) => {
      return (new Date(a.dueDate)).getTime() - (new Date(b.dueDate)).getTime();
    });
  }

  storeTask(): void {
    this.appService.setTask(this.filteredTask);
  }

  filterTask(key: string): void {
    this.filteredTask = this.tasks.filter((task) =>
      task.title.toLowerCase().includes(key.toLowerCase())
    );
  }

  onTaskSelect(id: string): void {
    if (!this.selectedTaskIds.includes(id)) {
      this.selectedTaskIds.push(id);
    } else {
      const index = this.selectedTaskIds.findIndex((taskId) => taskId === id);
      this.selectedTaskIds.splice(index, 1);
    }
  }

  removeTask(id: string): void {
    const taskIndex = this.tasks.findIndex((task) => task.id === id);
    const idIndex = this.selectedTaskIds.findIndex((task) => task.id === id);
    this.tasks.splice(taskIndex, 1);
    this.selectedTaskIds.splice(idIndex, 1);
    this.filterTask('');
    this.sortTask(this.filteredTask);
    this.storeTask();
  }

  bulkActionRemove(): void {
    const clone = [...this.selectedTaskIds];
    clone.forEach((id, index) => {
      this.removeTask(id);
      this.selectedTaskIds.splice(index, 1);
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
