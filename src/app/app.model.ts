export type Type = "add" | "task";
export interface Task {
  description: string;
  dueDate: string;
  id: string;
  piority: string;
  title: string;
}