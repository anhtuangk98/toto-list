FROM node:16 AS builder
WORKDIR /app
COPY . .
RUN npx ng build --configuration=production
FROM nginx:alpine
COPY --from=builder /app/dist/todo /usr/share/nginx/html
EXPOSE 8200
CMD ["nginx", "-g", "daemon off;"]